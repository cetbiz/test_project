FROM nginx:latest

# Install nginx process
#1. apt-get update
#2. apt install nginx -y

LABEL Maintainer="Sameer Sonaikar"
LABEL Email="sameer@cetbiz.com"

ENV MyName="sameer"
ENV abc=hello
ENV abc=bye def=$abc
ENV ghi=$abc


# Root : /usr/share/nginx/html
COPY code/index.html /usr/share/nginx/html/index.html